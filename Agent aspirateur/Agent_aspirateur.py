"""
IDENTIFICATION DES OBJETS

tab:
0: Vide
1: Poussière
2: Diamant
3: Diamant + poussière

sequence :
0 : gauche
1 : droite
2 : haut
3 : bas
4 : aspire
5 : ramasse
"""

"""
Editez la valeur ci dessous pour changer d'algorithme de recherche
0 : Algorithme exploration non informé (BFS)
1 : Algorithme exploration informé (A*)

"""
MODE =0


from tkinter import *
from random import randrange
from collections import defaultdict
from queue import PriorityQueue
from random import random
import threading , time

# region Parametre
# taille du terrain de simulation
global height
global width
height = 5
width = 5

#Timer pour generation poussiere + diamant
nextSpawnDuration = 3

# vitesse de l'aspirateur
vaccumActionDelay = 2

#endregion

# region Data
global nbDiamntsCollected
global nbPoussiereAspirer
global nbDiamantAspirer

nbDiamntsCollected = 0
nbPoussiereAspirer = 0
nbDiamantAspirer = 0
#endregion

#region INITIALISATION
########## INITIALISATION ##########

# Création de la fênêtre de simulation
window = Tk()
window.title = ("Agent aspirateur")

# Importation des sprites
tilesBackground = PhotoImage(file="bg_1.png")
vaccum = PhotoImage(file="aspi.png")
dirt = PhotoImage(file="poussiere.png")
diamond = PhotoImage(file="diamant.png")

# Création du terrain de simulation
canvas = Canvas(window, width=(width+2)*64, height= (height+1)*64)
canvasText = Canvas(window, width=(width+2)*64, height= 64)

# Création du tableau de positionement vide
global tab
tab = []
for x in range(width):
    tab.append([0]*height)

# Positionnement aléatoire de l'aspirateur
global vaccumPos
vaccumPos = [randrange(width),randrange(height)]

# Ajout d'une première poussière
tab[randrange(width)][randrange(height)] = 1;

# Ajout d'une premiere poussiere
tab[randrange(width)][randrange(height)] = 1;

# Ajout d'un diamant
i = randrange(width)
j = randrange(height)
if(tab[i][j] == 1):
	tab[i][j] = 3;
else:
	tab[i][j] = 2

# Premier affichage du terrain de simulation
i=0
j=0
for loop in range(width):
		for loop2 in range(height):
			# Affichage de l'arrière-plan
			canvas.create_image(i*64+64, j*64, anchor=NW, image=tilesBackground)
			# Affichage d'une poussière
			if(tab[i][j]==1):
				canvas.create_image(i*64+64, j*64, anchor=NW, image=dirt)
			# Affichage d'un diamant
			elif(tab[i][j] == 2):
				canvas.create_image(i*64+64, j*64, anchor=NW, image=diamond)
			# Affichage d'une poussière et d'un diamant
			elif(tab[i][j] == 3):
				canvas.create_image(i*64+64, j*64, anchor=NW, image=dirt)
				canvas.create_image(i*64+64, j*64, anchor=NW, image=diamond)
			# Affichage de l'aspirateur
			if((i == vaccumPos[0]) & (j == vaccumPos[1])):
				canvas.create_image(i*64+64, j*64, anchor=NW, image=vaccum)
			j = j + 1
		i = i + 1
		j=0
		
canvasText.pack()
canvas.pack()

########## FIN INITIALISATION ##########
#endregion

class House:
	def __init__(self):
		self.nextTime = 0

	def run(self):
		global tab
		# Initialisation du compteur de temps
		self.nextTime = time.time()
		self.initTime = time.time()


		# Teste si la simulation est en cours
		while (1):
			# Compteur de temps entre chaque ajout d'objet
			if (self.nextTime < time.time()):
				# Définition du temps pour la prochaine apparition d'un nouvel objet
				self.nextTime += nextSpawnDuration
				# Choix du positionnement du prochain objet à ajouter
				i = randrange(width)
				j = randrange(height)

				# Tentative d'ajout d'une poussière
				if(tab[i][j] == 0):
					# Ajout d'une poussière
					tab[i][j] = 2;
				elif(tab[i][j] != 2):
					# Ajout d'une poussière sur le diamant déjà présent
					tab[i][j] = 3
						
				i = randrange(width)
				j = randrange(height)
				# Tentative d'ajout d'un diamant
				if(tab[i][j] == 0):
					# Ajout d'un diamant
					tab[i][j] = 1;
				elif(tab[i][j] != 1):
					# Ajout d'un diamant sur la poussière déjà présente
					tab[i][j] = 3

#Necessaire pour l'exploration non informée
class Graph:
 
    # Constructeur
    def __init__(self):
        self.graph = defaultdict(list)
 
    # Fonction pour ajouter un sommet au graphe
    def AddEdge(self,u,v):
        self.graph[u].append(v)

#Necessaire pour l'exploration informée
class noeud:
	# Constructeur
	def __init__(self, i, j, cout, heur):
		self.x = i
		self.y = j 
		self.cout = cout
		self.heuristique = heur
	#Fonction d'ordonnancement pour la queue avec priorités, basée sur les heuristiques
	def __lt__(noeuda, noeudb):
		if ((noeuda.cout+noeuda.heuristique) < (noeudb.cout + noeudb.heuristique)):
			return True
		else : 
			return False

#Vérifie si un noeud est présent dans une liste
def PresentDansListe(liste, node) :
	for noeud in liste :
		if (noeud.x==node.x and noeud.y == node.y): 
			return True
	return False

class Agent:

	def __init__(self):
		# Création de la variable interne de l'environnement
		global tab
		self.agentTab = []
		#Création d'un indexage du graphe
		self.agentDict = defaultdict(list)

		#region Création du graphe
		# Création du graphe à partir du tableau d'environnement,
		# chaque noeud est relié à ses noeuds adjacents de 0 à 24
		self.graph = Graph()
		for i in range(5): # Liens horizontaux
			for j in range(4):
				self.graph.AddEdge(i*5+j,i*5+j+1)
				self.graph.AddEdge(i*5+j+1,i*5+j)
		for j in range(5): # Liens verticaux
			for i in range(4):
				self.graph.AddEdge(i*5+j,5+i*5+j)
				self.graph.AddEdge(5+i*5+j,i*5+j)
		
		#endregion
		# Stockage du cout d'une action
		self.cost = 0
		#pénalité pour absorbsion d'un diamant
		# Stockage de la séquence
		self.seq = []

	# Détermine les coordonnées du noeud dans le tableau d'environnement
	def GetPos(self, node):
		i = node // 5
		j = node % 5
		return [i,j]

	#Détermine la distance minimum en nombre de salles à parcourir entre 2 salles
	def Distance(self, xa, ya, xb, yb):
		return abs(xb-xa)+abs(yb-ya)

	# Détermine la séquence d'action de mouvement à partir des numéros des noeuds d'arrivée et de départ
	def GetAction(self, node, next):
		if next-node == 1: # Mouvement à droite
			return 1
		elif next-node == -1: # Mouvement à gauche
			return 0
		elif next-node == 5: # Mouvement en bas
			return 3
		elif next-node == -5: # Mouvement en haut
			return 2

	#Détermine le prochain mouvement (algorithme exploration informée)
	def SensNoeud(self, noeud1, noeud2):
		if (noeud1.x < noeud2.x) : # Mouvement à droite
			return 1
		elif (noeud1.x > noeud2.x): # Mouvement à gauche
			return 0
		elif (noeud1.y < noeud2.y) : # Mouvement en bas
			return 3
		elif (noeud1.y > noeud2.y): # Mouvement en haut
			return 2

	def GoLeft(self): #aller à gauche
		global vaccumPos
		if(vaccumPos[0]>0):
			vaccumPos[0] = vaccumPos[0]-1
			self.cost += 1

	def GoRight(self): #aller à droite
		global vaccumPos
		if(vaccumPos[0]<width-1):
			vaccumPos[0] = vaccumPos[0]+1
			self.cost += 1

	def GoUp(self): #aller vers le haut
		global vaccumPos
		if(vaccumPos[1] > 0):
			vaccumPos[1] = vaccumPos[1]-1
			self.cost += 1

	def GoDown(self): #aller vers le bas
		global vaccumPos
		if(vaccumPos[1]<height-1):
			vaccumPos[1] = vaccumPos[1]+1
			self.cost+=1

	def Vaccum(self): #On aspire tout
		global vaccumPos
		global tab
		global nbPoussiereAspirer
		global nbDiamantAspirer

		if(tab[vaccumPos[0]][vaccumPos[1]]==1): # aspiration d'une poussiere
			nbPoussiereAspirer += 1
		if(tab[vaccumPos[0]][vaccumPos[1]]==2): # aspiration d'un diamant
			nbDiamantAspirer += 1
		if(tab[vaccumPos[0]][vaccumPos[1]]==3): # aspiration d'une poussiere et d'un diamant
			nbDiamantAspirer += 1
			nbPoussiereAspirer += 1
		tab[vaccumPos[0]][vaccumPos[1]] = 0
		self.cost += 1

	def Collect(self): #fonction de ramassage d'un diamant
		global vaccumPos
		global tab
		global nbDiamntsCollected

		if (tab[vaccumPos[0]][vaccumPos[1]] == 2): #On ramasse le diamant seul
			tab[vaccumPos[0]][vaccumPos[1]] = 0
			nbDiamntsCollected += 1
		elif(tab[vaccumPos[0]][vaccumPos[1]] == 3): #On ramasse le diamant en laissant la poussiere
			tab[vaccumPos[0]][vaccumPos[1]] = 1
			nbDiamntsCollected += 1
		self.cost +=1

	def AmIAlive(self, initTime):
		global nextTime
		nextTime = time.time()
		self.answer = True
		if (nextTime >= initTime + 100): #Condition d'arrêt au bout de 100 secondes
			self.answer = False
		return self.answer

	#scan de l'environnement par l'aspirateur
	def ObserveEnvironnment(self):
		global tab
		if (MODE==1): #Tableau pour l'exploration informée
			self.agentinformeTab = tab
		if(MODE==0): #graph pour l'exploration non informée
			self.agentTab = []
			for i in range(width):
				for j in range(height):
					self.agentTab.append(tab[j][i])
			# Transformation du tableau en dictionnaire de valeurs (chaque case est numéroté de 0 à 24 avec sa valeur d'identification)
			self.agentDict = defaultdict(list)
			self.temp = 0
			for values in self.agentTab:
				self.agentDict[self.temp].append(values)
				self.temp += 1
		print("scanned")

	def UpdateMyState(self):
		print("update")

	# Recherche et décision de la séquence d'action à faire
	# Mode: "0: non informé" ou "1: informé"
	# 
	def ChooseAnAction(self, mode):

		global vaccumPos
		node = vaccumPos[1] * 5 + vaccumPos[0] # Noeud de départ, position de l'aspirateur

		# Recherche non informée
		if mode == 0:
			print("Suivi de l'algorithme Breadth First Search en cours")
			# Implémentation du Breadth First Search (BFS)
			# Marquage des noeuds non visités
			visited = [False] * (25)

			# Création d'une file pour le BFS
			queue = []

			# Création d'une boucle d'arrêt
			noGoal = True

			# Marquage du noeud de départ comme visité et dans la file
			queue.append(node)
			visited[node] = True
			# Vérification de l'état du noeud
			state = self.agentTab[node]
			if state == 1 : # Poussière détectée, ajout de la séquence d'action d'aspirateur
				print("poussière détectée à l'origine")
				noGoal = False
				self.seq.append(4)
			elif state == 2: # Diamant détecté, ajout de la séquence d'action de ramassage
				print("diamant détectée à l'origine")
				noGoal = False
				self.seq.append(5)
			elif state == 3: # Diamant et poussière détectée, ajout de la séquence d'action de ramassage et d'aspirateur
				print("poussière et diamant détectés à l'origine")
				noGoal = False
				self.seq.append(5)
				self.seq.append(4)

			while noGoal and queue: # S'arrête quand un but est trouvé ou que toutes les pièces soient vérifiées

				# Sortie d'un noeud depuis la file et affichage
				node = queue.pop(0)
				print(node, end = " ")
				
				# Récupération des noeuds adjacents au noeud récupéré
				# Si le noeuf n'a pas été visité, le marque et l'enfile dans la séquence d'actions
				for next in self.graph.graph[node]:
					if visited[next] == False:
						queue.append(next)
						visited[next] = True
						# Vérification de l'état du noeud 
						state = self.agentTab[next]
						# Ajout de la séquence d'action à effectuer
						if state == 0: # Aucune poussière, mais on se déplace quand même
							print("rien de détectée")
							self.seq.append(self.GetAction(node,next))
							node = next
							break
						elif state == 1 : # Poussière détectée, ajout de la séquence d'action de mouvement et d'aspirateur
							print("poussière détectée")
							noGoal = False
							self.seq.append(self.GetAction(node,next))
							self.seq.append(4)
							break
						elif state == 2: # Diamant détecté, ajout de la séquence d'action de mouvement et de ramassage
							print("diamant détectée")
							noGoal = False
							self.seq.append(self.GetAction(node,next))
							self.seq.append(5)
							break
						elif state == 3: # Diamant et poussière détectée, ajout de la séquence d'action de mouvement, de ramassage et d'aspirateur
							print("poussière et diamant détectés")
							noGoal = False
							self.seq.append(self.GetAction(node,next))
							self.seq.append(5)
							self.seq.append(4)
							break
			print(self.seq)

		#Recherche informée
		elif mode == 1:
			coorpoussiere = [] #tableau de coordonnées des poussières et leur distance avec l'aspirateur
			ipoussiere =0 #indice i d'une poussière dans le tableau
			print("Suivi de l'algorithme A* en cours")
			# Implémentation du A*
			#récupération des coordonnées des poussières
			for i in range(width):
					for j in range(height):
						if (self.agentinformeTab[i][j] ==1 or self.agentinformeTab[i][j]==3):
							coorpoussiere.append([])
							coorpoussiere[ipoussiere].append(i)
							coorpoussiere[ipoussiere].append(j)
							coorpoussiere[ipoussiere].append(self.Distance(vaccumPos[0], vaccumPos[1], i, j))
							ipoussiere +=1

			#Trie des poussières en fonction de la distance par rapport à l'aspirateur
			coorpoussiere.sort(key=lambda x:x[2])

			#Poussière la plus proche mise en valeur
			PremierePoussiere = noeud(coorpoussiere[0][0], coorpoussiere[0][1], 1, 0)			

			#Début de l'algorithme A* pour aller au plus vite vers la poussière la plus proche
			closed = []
			queue = PriorityQueue()
			#La position de l'aspirateur sert de noeud de départ
			depart=noeud(vaccumPos[0], vaccumPos[1], 0, self.Distance(vaccumPos[0], vaccumPos[1], PremierePoussiere.x, PremierePoussiere.y))
			queue.put((0,depart))
			#tant qu'il reste des noeuds à explorer
			while not queue.empty() :
				NoeudCourant= queue.get()[1]
				#Si on se trouve sur le noeud de la poussière
				if (NoeudCourant.x == PremierePoussiere.x and NoeudCourant.y == PremierePoussiere.y) : 
					if (self.agentinformeTab[PremierePoussiere.x][PremierePoussiere.y]==1): #Si l'agent est sur de la poussiere
						self.seq.append(4)
					else : #Ramassage du diamant et aspiration
						self.seq.append(5)
						self.seq.append(4)
					return #On sort de la fonction quand la poussière est aspi pour lancer la séquence d'action
				else:
					#On regarde les voisins autour
					voisins = []
					if (NoeudCourant.x < height) : # check en bas
						voisins.append(noeud(NoeudCourant.x+1, NoeudCourant.y, NoeudCourant.cout+1, self.Distance(NoeudCourant.x+1,NoeudCourant.y, PremierePoussiere.x, PremierePoussiere.y)))

					if (NoeudCourant.x > 0): # check en haut
						voisins.append(noeud(NoeudCourant.x-1, NoeudCourant.y, NoeudCourant.cout+1, self.Distance(NoeudCourant.x-1,NoeudCourant.y, PremierePoussiere.x, PremierePoussiere.y)))

					if (NoeudCourant.y < width) : # check à droite
						voisins.append(noeud(NoeudCourant.x, NoeudCourant.y+1, NoeudCourant.cout+1, self.Distance(NoeudCourant.x,NoeudCourant.y+1, PremierePoussiere.x, PremierePoussiere.y)))

					if (NoeudCourant.y > 0) : # check à gauche
						voisins.append(noeud(NoeudCourant.x, NoeudCourant.y-1, NoeudCourant.cout+1, self.Distance(NoeudCourant.x,NoeudCourant.y-1, PremierePoussiere.x, PremierePoussiere.y)))

					#On regarde les voisins qui permettent d'approcher la poussière
					for voisin in voisins:
						#On regarde si le voisin en cours n'est pas un noeud déjà visité
						if (not PresentDansListe(closed, voisin)) :
							#Si non visité, on met le voisin dans la queue
							queue.put(((voisin.cout+voisin.heuristique), voisin))
							
				#On ajoute le mouvement qui va permettre d'aller du noeud courant au prochain noeud de la queue
				self.seq.append(self.SensNoeud(NoeudCourant, queue.queue[0][1]))
				closed.append(NoeudCourant)
			print("poussière non repérable")

	#Fonction qui execute la séquence créé dans ChooseAnAction
	def JustDoIt(self):
		global vaccumPos
		while (len(self.seq) != 0) :
			if(self.seq[0]==0):
				self.GoLeft()
				time.sleep(vaccumActionDelay)
			elif(self.seq[0]==1):
				self.GoRight()
				time.sleep(vaccumActionDelay)
			elif(self.seq[0]==2):
				self.GoUp()
				time.sleep(vaccumActionDelay)
			elif(self.seq[0]==3):
				self.GoDown()
				time.sleep(vaccumActionDelay)
			elif(self.seq[0]==4):
				self.Vaccum()
				time.sleep(vaccumActionDelay)
			elif(self.seq[0]==5):
				self.Collect()
				time.sleep(vaccumActionDelay)
			del self.seq[0]
			print("Position :" + str(vaccumPos[1] * 5 + vaccumPos[0]))
		print("cout: " + str(self.cost))

	def run(self):
		global tab
		global vaccumPos

		# Initalisation timer
		self.initTime = time.time()
		nextTime = time.time()

		while (self.AmIAlive(self.initTime)):
			if (nextTime < time.time()): #Timer entre chaque boucle d'agent
				nextTime += 3
				self.ObserveEnvironnment()
				self.UpdateMyState()
				self.ChooseAnAction(MODE) # Recherche non informée (MODE = 0) / informée (MODE = 1)
				self.JustDoIt()


class CanvasDraw:
	def __init__(self):
		pass

	def run(self):
		# Simulation de la boucle
		while (1):
				# Début d'affichage du terrain
				i=0
				j=0
				
				canvasText.delete('all')
				canvasText.create_text(100,10,fill="darkblue",font="Times 10",
                        text="Collected diamond : " + str(nbDiamntsCollected))
				canvasText.create_text(100,30,fill="darkblue",font="Times 10",
                        text="Aspirated dust: " + str(nbPoussiereAspirer))
				canvasText.create_text(100,50,fill="darkblue",font="Times 10",
                        text="Aspirated diamond : " + str(nbDiamantAspirer))

				for loop in range(width):
					for loop2 in range(height):
						# Affichage de l'arrière-plan
						canvas.create_image(i*64+64, j*64, anchor=NW, image=tilesBackground)
						if(tab[i][j]==1):
							# Affichage d'une poussière
							canvas.create_image(i*64+64, j*64, anchor=NW, image=dirt)
						elif(tab[i][j] == 2):
							# Affichage d'un diamant
							canvas.create_image(i*64+64, j*64, anchor=NW, image=diamond)
						elif(tab[i][j] == 3):
							# Affichage d'une poussière + diamant
							canvas.create_image(i*64+64, j*64, anchor=NW, image=dirt)
							canvas.create_image(i*64+64, j*64, anchor=NW, image=diamond)
						if((i == vaccumPos[0]) & (j == vaccumPos[1])):
							# Affichage de l'agent aspirateur
							canvas.create_image(i*64+64, j*64, anchor=NW, image=vaccum)
						j = j + 1
					i = i + 1
					j=0
				canvasText.update()
				canvas.update()
				time.sleep(0.16)

#region LANCEMENT DES THREADS
########## LANCEMENT DES THREADS ##########

# Création des objets de simulation
house = House()
agent = Agent()
draw = CanvasDraw()

# Création des threads
houseTread = threading.Thread(target=house.run, args=())
agentTread = threading.Thread(target=agent.run, args=())
drawTread = threading.Thread(target=draw.run, args=())
# Lancement des threads
houseTread.start()
agentTread.start()
drawTread.start()

# Lancement de la fenêtre
window.mainloop()

########## FIN LANCEMENT DES THREADS ##########
#endregion
